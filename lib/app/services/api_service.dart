import 'dart:async';
import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';

import '../models/movie_response.dart';
import '../services/dio_config_service.dart' as dio_config;

class ApiServices {
  Future<MovieResponse?> getMovieList(page) async {
    try {
      var dio = await (dio_config.dio(page));
      Response<String> response = await dio.get("");
      MovieResponse movieResponse =
          MovieResponse.fromJson(jsonDecode(response.data!));
      return movieResponse;
    } catch (e) {
      debugPrint(e.toString());
      return null;
    }
  }
}
