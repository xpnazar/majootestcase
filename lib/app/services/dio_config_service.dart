import 'dart:async';
import 'package:dio/dio.dart';

Dio? dioInstance;
const apiKey = "59bdbc058bf8a49a02af0a7afb1b5326";
const baseURL = "https://api.themoviedb.org/3/trending/all/day?api_key=$apiKey";
createInstance(int page) async {
  var options = BaseOptions(
    baseUrl: '$baseURL&page=$page',
    connectTimeout: 12000,
    receiveTimeout: 12000,
  );
  dioInstance = Dio(options);
}

FutureOr<Dio> dio(int page) async {
  await createInstance(page);
  dioInstance?.options.baseUrl = '$baseURL&page=$page';
  return dioInstance ?? Dio();
}
