import 'dart:convert';

const String tableUser = 'users';

class User {
  int? id;
  String? fullname;
  String email, password;

  User({
    this.id,
    this.fullname,
    required this.email,
    required this.password,
  });

  User copy({
    int? id,
    String? fullname,
    String? email,
    String? password,
  }) =>
      User(
        id: id ?? this.id,
        email: email ?? this.email,
        fullname: fullname ?? this.fullname,
        password: password ?? this.password,
      );

  User.fromJson(Map<String, dynamic> json)
      : id = json['id'],
        email = json['email'],
        password = json['password'],
        fullname = json['fullname'];

  Map<String, dynamic> toJson() =>
      {'email': email, 'password': password, 'fullname': fullname};

  static Map<String, dynamic> toMap(User user) => {
        'id': user.id,
        'fullname': user.fullname,
        'email': user.email,
        'password': user.password,
      };

  static String encode(List<User> users) => json.encode(
        users.map<Map<String, dynamic>>((user) => User.toMap(user)).toList(),
      );

  static List<User> decode(String users) =>
      (json.decode(users) as List<dynamic>)
          .map<User>((item) => User.fromJson(item))
          .toList();
}
