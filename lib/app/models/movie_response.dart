class MovieResponse {
  List<Data> data;
  int page, totalPages, totalResults;

  MovieResponse({
    required this.data,
    required this.page,
    required this.totalPages,
    required this.totalResults,
  });

  factory MovieResponse.fromJson(Map<String, dynamic> json) {
    return MovieResponse(
      data: List<Data>.from(json["results"].map((x) => Data.fromJson(x))),
      page: json["page"],
      totalPages: json["total_pages"],
      totalResults: json["total_results"],
    );
  }

  Map<String, dynamic> toJson() => {
        "results": List<dynamic>.from(data.map((x) => x.toJson())),
        "page": page,
        "total_pages": totalPages,
        "total_results": totalResults,
      };
}

class Data {
  int id, voteCount;
  double voteAverage, popularity;
  bool adult, video;
  dynamic genreIDs;
  String? backdropPath, posterPath, title, originalTitle, originalLanguage;
  String? overview, releaseDate, mediaType;

  Data({
    required this.id,
    required this.voteCount,
    required this.voteAverage,
    required this.popularity,
    required this.adult,
    required this.video,
    required this.genreIDs,
    required this.backdropPath,
    required this.posterPath,
    required this.title,
    required this.originalTitle,
    required this.originalLanguage,
    required this.overview,
    required this.releaseDate,
    required this.mediaType,
  });

  factory Data.fromJson(Map<String, dynamic> json) {
    return Data(
      id: json['id'],
      voteCount: json['vote_count'],
      voteAverage: json['vote_average'],
      popularity: json['popularity'],
      adult: json['adult'] ?? false,
      video: json['video'] ?? false,
      genreIDs: json['genre_ids'],
      backdropPath: json['backdrop_path'],
      posterPath: json['poster_path'],
      title: json['title'] ?? json['name'],
      originalTitle: json['original_title'] ?? json['original_name'],
      originalLanguage: json['original_language'],
      overview: json['overview'],
      releaseDate: json['release_date'] ?? '-',
      mediaType: json['media_type'],
    );
  }

  Map<String, dynamic> toJson() => {
        'id': id,
        'vote_count': voteCount,
        'vote_average': voteAverage,
        'popularity': popularity,
        'adult': adult,
        'video': video,
        'genre_ids': genreIDs,
        'backdrop_path': backdropPath,
        'poster_path': posterPath,
        'title': title,
        'original_title': originalTitle,
        'original_language': originalLanguage,
        'overview': overview,
        'release_date': releaseDate,
        'media_type': mediaType,
      };
}
