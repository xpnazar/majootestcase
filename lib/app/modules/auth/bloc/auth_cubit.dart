import 'dart:async';

import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../../database/user_database.dart';
import '../../../models/user.dart';
import '../../home/bloc/home_cubit.dart';
import '../../home/view/home_screen.dart';

part 'auth_state.dart';

class AuthBlocCubit extends Cubit<AuthBlocState> {
  AuthBlocCubit() : super(AuthBlocInitialState());

  void fetchHistoryLogin() async {
    emit(AuthBlocInitialState());
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    bool? isLoggedIn = sharedPreferences.getBool("is_logged_in");
    if (isLoggedIn == null) {
      emit(AuthBlocLoginState());
    } else {
      if (isLoggedIn) {
        emit(AuthBlocLoggedInState());
      } else {
        emit(AuthBlocLoginState());
      }
    }
  }

  loading(BuildContext context) {
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        return const AlertDialog(
          backgroundColor: Colors.transparent,
          content: Center(
            child: CircularProgressIndicator(
              color: Colors.red,
              strokeWidth: 35,
            ),
          ),
        );
      },
    );
  }

  void loginUser(User user, BuildContext context) async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    loading(context);
    Timer(const Duration(seconds: 1), () async {
      emit(AuthBlocLoadingState());
      final login =
          await UsersDatabase.instance.login(user.email, user.password);
      Navigator.pop(context);
      if (login == 0) {
        ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
          content: Text("Login gagal, akun tidak ditemukan"),
          duration: Duration(seconds: 3),
        ));
      } else if (login == 1) {
        ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
          content: Text("Login gagal, password salah"),
          duration: Duration(seconds: 3),
        ));
      } else {
        await sharedPreferences.setBool("is_logged_in", true);
        // Encode and store data in SharedPreferences
        final String data = User.encode([login]);
        sharedPreferences.setString("user_value", data);
        Navigator.pushAndRemoveUntil(
          context,
          MaterialPageRoute(
            builder: (_) => BlocProvider(
              create: (context) => HomeBlocCubit()..fetchingData(context),
              child: const HomeBlocView(),
            ),
          ),
          (Route<dynamic> route) => false,
        );
      }
      emit(AuthBlocLoggedInState());
    });
  }

  void registerUser(User user, BuildContext context) async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    loading(context);
    Timer(const Duration(seconds: 1), () async {
      emit(AuthBlocLoadingState());
      final register = await UsersDatabase.instance.register(user);
      Navigator.pop(context);
      if (register != null) {
        await sharedPreferences.setBool("is_logged_in", true);
        // Encode and store data in SharedPreferences
        final String data = User.encode([register]);
        sharedPreferences.setString("user_value", data);
        Navigator.pushAndRemoveUntil(
          context,
          MaterialPageRoute(
            builder: (_) => BlocProvider(
              create: (context) => HomeBlocCubit()..fetchingData(context),
              child: const HomeBlocView(),
            ),
          ),
          (Route<dynamic> route) => false,
        );
      } else {
        ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
          content: Text("Registrasi gagal, alamat email sudah digunakan"),
          duration: Duration(seconds: 3),
        ));
      }
      emit(AuthBlocLoggedInState());
    });
  }
}
