part of 'auth_cubit.dart';

abstract class AuthBlocState extends Equatable {
  const AuthBlocState();

  @override
  List<Object> get props => [];
}

class AuthBlocInitialState extends AuthBlocState {}

class AuthBlocLoadingState extends AuthBlocState {}

class AuthBlocLoggedInState extends AuthBlocState {}

class AuthBlocLoginState extends AuthBlocState {}

class AuthBlocSuccesState extends AuthBlocState {}

class AuthBlocLoadedState extends AuthBlocState {
  const AuthBlocLoadedState(this.data);
  final dynamic data;

  @override
  List<Object> get props => [data];
}

class AuthBlocErrorState extends AuthBlocState {
  const AuthBlocErrorState(this.error);
  final dynamic error;

  @override
  List<Object> get props => [error];
}
