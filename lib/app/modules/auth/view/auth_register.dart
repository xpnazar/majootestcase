import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../common/widget/custom_button.dart';
import '../../../common/widget/text_form_field.dart';
import '../../../models/user.dart';
import '../../home/bloc/home_cubit.dart';
import '../../home/view/home_screen.dart';
import '../bloc/auth_cubit.dart';

class RegisterPage extends StatefulWidget {
  const RegisterPage({Key? key}) : super(key: key);

  @override
  _RegisterState createState() => _RegisterState();
}

class _RegisterState extends State<RegisterPage> {
  final _fullnameController = TextController();
  final _emailController = TextController();
  final _passwordController = TextController();
  GlobalKey<FormState> formKey = GlobalKey<FormState>();
  bool _isObscurePassword = true;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      body: GestureDetector(
        behavior: HitTestBehavior.opaque,
        onTap: () => FocusScope.of(context).requestFocus(FocusNode()),
        child: BlocListener<AuthBlocCubit, AuthBlocState>(
          listener: (context, state) {
            if (state is AuthBlocLoggedInState) {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (_) => BlocProvider(
                    create: (context) => HomeBlocCubit()..fetchingData(context),
                    child: const HomeBlocView(),
                  ),
                ),
              );
            }
          },
          child: ScrollConfiguration(
            behavior: const ScrollBehavior().copyWith(overscroll: false),
            child: Center(
              child: ListView(
                shrinkWrap: true,
                padding: const EdgeInsets.only(
                    top: 75, left: 25, bottom: 25, right: 25),
                children: <Widget>[
                  const Text(
                    'Registrasi Akun',
                    style: TextStyle(
                      fontSize: 24,
                      fontWeight: FontWeight.bold,
                      color: Colors.white,
                    ),
                  ),
                  const Text(
                    'Silahkan isi form dibawah ini untuk mendaftar',
                    style: TextStyle(
                      fontSize: 15,
                      fontWeight: FontWeight.w400,
                      color: Colors.white,
                    ),
                  ),
                  _form(),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 75),
                    child: CustomButton(
                      text: 'Register',
                      onPressed: handleRegister,
                      height: 100,
                    ),
                  ),
                  const SizedBox(height: 10),
                  _register(),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget _form() {
    return Form(
      key: formKey,
      child: Column(
        children: [
          const SizedBox(height: 20),
          CustomTextFormField(
            context: context,
            controller: _fullnameController,
            label: 'Nama Lengkap',
            hint: 'nama lengkap anda',
            validator: (val) {
              if (val!.length < 3) {
                return 'Nama lengkap minimal harusNaza 3 karakter';
              } else {
                return null;
              }
            },
          ),
          const SizedBox(height: 10),
          CustomTextFormField(
            context: context,
            controller: _emailController,
            isEmail: true,
            label: 'Email',
            hint: 'example@mail.com',
            validator: (val) {
              final pattern = RegExp(r'([\d\w]{1,}@[\w\d]{1,}\.[\w]{1,})');
              if (val != null) {
                return pattern.hasMatch(val)
                    ? null
                    : 'Masukkan e-mail yang valid';
              }
              return null;
            },
          ),
          const SizedBox(height: 10),
          CustomTextFormField(
            context: context,
            label: 'Password',
            hint: 'password minimal 6 karakter',
            controller: _passwordController,
            isObscureText: _isObscurePassword,
            textInputAction: TextInputAction.done,
            validator: (val) {
              if (val!.length < 6) {
                return 'Password minimal harus 6 karakter';
              } else {
                return null;
              }
            },
            suffixIcon: IconButton(
              icon: Icon(
                _isObscurePassword
                    ? Icons.visibility_off_outlined
                    : Icons.visibility_outlined,
                size: 18,
              ),
              onPressed: () {
                setState(() {
                  _isObscurePassword = !_isObscurePassword;
                });
              },
            ),
          ),
          const SizedBox(height: 20),
        ],
      ),
    );
  }

  Widget _register() {
    return Align(
      alignment: Alignment.center,
      child: RichText(
        textAlign: TextAlign.center,
        text: TextSpan(
          style: const TextStyle(color: Colors.white70, fontSize: 14),
          children: <TextSpan>[
            const TextSpan(text: 'Sudah punya akun? '),
            TextSpan(
              text: 'Login Disini.',
              style: const TextStyle(
                color: Colors.blue,
              ),
              recognizer: TapGestureRecognizer()
                ..onTap = () => Navigator.pop(context),
            ),
          ],
        ),
      ),
    );
  }

  void handleRegister() async {
    final String? _fullname = _fullnameController.value;
    final String? _email = _emailController.value;
    final String? _password = _passwordController.value;
    FocusScope.of(context).requestFocus(FocusNode());
    if (formKey.currentState?.validate() == true &&
        _fullname != null &&
        _email != null &&
        _password != null) {
      AuthBlocCubit authBlocCubit = AuthBlocCubit();
      User user = User(fullname: _fullname, email: _email, password: _password);
      authBlocCubit.registerUser(user, context);
    } else {
      if (_fullname == null && _email == null && _password == null) {
        ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
          content: Text(
              "Form tidak boleh kosong, mohon cek kembali data yang anda inputkan"),
          duration: Duration(seconds: 2),
        ));
      }
    }
  }
}
