import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../common/widget/custom_button.dart';
import '../../../common/widget/text_form_field.dart';
import '../../../models/user.dart';
import '../../home/bloc/home_cubit.dart';
import '../../home/view/home_screen.dart';
import '../bloc/auth_cubit.dart';
import 'auth_register.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<LoginPage> {
  final _emailController = TextController();
  final _passwordController = TextController();
  GlobalKey<FormState> formKey = GlobalKey<FormState>();
  bool _isObscurePassword = true;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      body: GestureDetector(
        behavior: HitTestBehavior.opaque,
        onTap: () => FocusScope.of(context).requestFocus(FocusNode()),
        child: BlocListener<AuthBlocCubit, AuthBlocState>(
          listener: (context, state) {
            if (state is AuthBlocLoggedInState) {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (_) => BlocProvider(
                    create: (context) => HomeBlocCubit()..fetchingData(context),
                    child: const HomeBlocView(),
                  ),
                ),
              );
            }
          },
          child: ScrollConfiguration(
            behavior: const ScrollBehavior().copyWith(overscroll: false),
            child: Padding(
              padding: const EdgeInsets.only(
                  top: 75, left: 25, bottom: 25, right: 25),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  const Spacer(),
                  const Text(
                    'Selamat Datang',
                    style: TextStyle(
                      fontSize: 24,
                      fontWeight: FontWeight.bold,
                      color: Colors.white,
                    ),
                  ),
                  const Text(
                    'Silahkan login terlebih dahulu',
                    style: TextStyle(
                      fontSize: 15,
                      fontWeight: FontWeight.w400,
                      color: Colors.white,
                    ),
                  ),
                  _form(),
                  Center(
                    child: CustomButton(
                      text: 'Login',
                      onPressed: handleLogin,
                      height: 100,
                    ),
                  ),
                  const Spacer(),
                  _register(),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget _form() {
    return Form(
      key: formKey,
      child: Column(
        children: [
          const SizedBox(height: 20),
          CustomTextFormField(
            context: context,
            controller: _emailController,
            isEmail: true,
            label: 'Email',
            hint: 'example@mail.com',
            validator: (val) {
              final pattern = RegExp(r'([\d\w]{1,}@[\w\d]{1,}\.[\w]{1,})');
              if (val != null) {
                return pattern.hasMatch(val)
                    ? null
                    : 'Masukkan e-mail yang valid';
              } else {
                return null;
              }
            },
          ),
          const SizedBox(height: 10),
          CustomTextFormField(
            context: context,
            label: 'Password',
            hint: 'password',
            controller: _passwordController,
            isObscureText: _isObscurePassword,
            textInputAction: TextInputAction.done,
            validator: (val) {
              if (val == "") {
                return 'Password tidak boleh kosong';
              } else {
                return null;
              }
            },
            suffixIcon: IconButton(
              icon: Icon(
                _isObscurePassword
                    ? Icons.visibility_off_outlined
                    : Icons.visibility_outlined,
                size: 18,
              ),
              onPressed: () {
                setState(() {
                  _isObscurePassword = !_isObscurePassword;
                });
              },
            ),
          ),
          const SizedBox(height: 20),
        ],
      ),
    );
  }

  Widget _register() {
    return Align(
      alignment: Alignment.center,
      child: RichText(
        textAlign: TextAlign.center,
        text: TextSpan(
          style: const TextStyle(color: Colors.white70, fontSize: 14),
          children: <TextSpan>[
            const TextSpan(text: 'Belum punya akun? '),
            TextSpan(
              text: 'Daftar Disini.',
              style: const TextStyle(
                color: Colors.blue,
              ),
              recognizer: TapGestureRecognizer()
                ..onTap = () => Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (_) => BlocProvider(
                          create: (context) =>
                              AuthBlocCubit()..fetchHistoryLogin(),
                          child: const RegisterPage(),
                        ),
                      ),
                    ),
            ),
          ],
        ),
      ),
    );
  }

  void handleLogin() async {
    final String? _email = _emailController.value;
    final String? _password = _passwordController.value;
    FocusScope.of(context).requestFocus(FocusNode());
    if (formKey.currentState?.validate() == true &&
        _email != null &&
        _password != null) {
      AuthBlocCubit authBlocCubit = AuthBlocCubit();
      User user = User(email: _email, password: _password);
      authBlocCubit.loginUser(user, context);
    } else {
      if (_email == null && _password == null) {
        ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
          content: Text(
              "Form tidak boleh kosong, mohon cek kembali data yang anda inputkan"),
          duration: Duration(seconds: 2),
        ));
      }
    }
  }
}
