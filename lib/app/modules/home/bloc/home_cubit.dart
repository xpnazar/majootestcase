import 'dart:io';

import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../../models/movie_response.dart';
import '../../../models/user.dart';
import '../../../services/api_service.dart';
import '../../auth/bloc/auth_cubit.dart';
import '../../auth/view/auth_login.dart';

part 'home_state.dart';

class HomeBlocCubit extends Cubit<HomeBlocState> {
  HomeBlocCubit() : super(HomeBlocInitialState());
  int page = 1;
  ApiServices apiServices = ApiServices();

  Future fetchingData(BuildContext context) async {
    emit(HomeBlocInitialState());
    try {
      final result = await InternetAddress.lookup('google.com');
      if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
        MovieResponse? movieResponse = await apiServices.getMovieList(1);
        if (movieResponse == null) {
          emit(const HomeBlocErrorState(
              "an error occured, please try again later"));
        } else {
          emit(HomeBlocLoadedState(movieResponse.data));
        }
      }
    } on SocketException catch (_) {
      emit(const HomeBlocErrorState("no internet connection"));
    }
  }

  Future loadMore(BuildContext context) async {
    try {
      final result = await InternetAddress.lookup('google.com');
      if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
        AuthBlocCubit().loading(context);
        MovieResponse? movieResponse = await apiServices.getMovieList(page);
        Navigator.pop(context);
        if (movieResponse == null) {
          emit(const HomeBlocErrorState(
              "an error occured, please try again later"));
        } else {
          emit(HomeBlocLoadedState(movieResponse.data));
        }
      }
    } on SocketException catch (_) {
      emit(const HomeBlocErrorState("no internet connection"));
    }
  }

  void handleClick(String value, BuildContext context) {
    switch (value) {
      case 'Logout':
        signOut(context);
        break;
      case 'Profil':
        profile(context);
        break;
    }
  }

  void profile(BuildContext context) async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    final String? data = sharedPreferences.getString('user_value');
    final List<User> users = User.decode(data!);
    final user = users[0];
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          contentPadding: const EdgeInsets.fromLTRB(20, 20, 20, 0),
          content: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              const Text(
                'PROFIL',
                style: TextStyle(
                  fontSize: 20,
                  fontWeight: FontWeight.bold,
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 10),
                child: Align(
                  alignment: Alignment.centerLeft,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'Nama : ${user.fullname}',
                        style: const TextStyle(fontSize: 15),
                      ),
                      Text(
                        'E-Mail : ${user.email}',
                        style: const TextStyle(fontSize: 15),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
          actions: <Widget>[
            TextButton(
              onPressed: () {
                Navigator.of(context).pop();
              },
              child: const Text(
                'Tutup',
                style: TextStyle(
                  color: Colors.blue,
                  fontSize: 15,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
          ],
        );
      },
    );
  }

  void signOut(BuildContext context) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          contentPadding: const EdgeInsets.fromLTRB(20, 20, 20, 0),
          content: Column(
            mainAxisSize: MainAxisSize.min,
            children: const [
              Text(
                'PEMBERITAHUAN',
                style: TextStyle(
                  fontSize: 20,
                  fontWeight: FontWeight.bold,
                ),
              ),
              Padding(
                padding: EdgeInsets.only(top: 10),
                child: Align(
                  alignment: Alignment.centerLeft,
                  child: Text(
                    'Apakah anda yakin ingin logout? Anda tidak akan bisa mengakses berbagai fitur sebelum login kembali.',
                    maxLines: 4,
                    overflow: TextOverflow.ellipsis,
                    textAlign: TextAlign.justify,
                    style: TextStyle(
                      color: Colors.black,
                      fontSize: 15,
                    ),
                  ),
                ),
              ),
            ],
          ),
          actions: <Widget>[
            TextButton(
              child: const Text(
                'Ya',
                style: TextStyle(
                  color: Color(0xFFD73C29),
                  fontSize: 15,
                  fontWeight: FontWeight.bold,
                ),
              ),
              onPressed: () async {
                SharedPreferences prefs = await SharedPreferences.getInstance();
                prefs.clear();
                Navigator.pushAndRemoveUntil(
                  context,
                  MaterialPageRoute(
                    builder: (_) => BlocProvider(
                      create: (context) => AuthBlocCubit()..fetchHistoryLogin(),
                      child: const LoginPage(),
                    ),
                  ),
                  (Route<dynamic> route) => false,
                );
              },
            ),
            TextButton(
              child: const Text(
                'Tidak',
                style: TextStyle(
                  color: Colors.blue,
                  fontSize: 15,
                  fontWeight: FontWeight.bold,
                ),
              ),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }
}
