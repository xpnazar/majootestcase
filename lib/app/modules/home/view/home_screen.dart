import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../common/extra/error_screen.dart';
import '../../../common/extra/loading.dart';
import '../bloc/home_cubit.dart';
import 'home_list.dart';

class HomeBlocView extends StatelessWidget {
  const HomeBlocView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<HomeBlocCubit, HomeBlocState>(
      builder: (context, state) {
        if (state is HomeBlocLoadedState) {
          return MovieList(data: state.data);
        } else if (state is HomeBlocInitialState ||
            state is HomeBlocLoadingState) {
          return const LoadingIndicator();
        } else if (state is HomeBlocErrorState) {
          bool isNetworkError = '${state.error}'.contains('no internet');
          return ErrorScreen(
            message: state.error,
            icon: !isNetworkError ? null : Icons.wifi_off,
            retryButton: !isNetworkError
                ? const SizedBox()
                : ElevatedButton(
                    onPressed: () =>
                        context.read<HomeBlocCubit>().fetchingData(context),
                    style: ElevatedButton.styleFrom(
                      primary: Colors.red,
                      shape: const RoundedRectangleBorder(
                        borderRadius: BorderRadius.all(Radius.circular(10)),
                      ),
                    ),
                    child: const Text(
                      "REFRESH",
                      style: TextStyle(fontWeight: FontWeight.bold),
                    ),
                  ),
          );
        } else {
          return Center(
            child: Text(kDebugMode ? "state not implemented $state" : ""),
          );
        }
      },
    );
  }
}
