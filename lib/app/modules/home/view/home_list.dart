import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:majootestcase/app/modules/home/bloc/home_cubit.dart';
import 'package:staggered_grid_view_flutter/widgets/staggered_grid_view.dart';
import 'package:staggered_grid_view_flutter/widgets/staggered_tile.dart';

import '../../../models/movie_response.dart';
import 'home_detail.dart';

class MovieList extends StatelessWidget {
  const MovieList({Key? key, this.data}) : super(key: key);
  final List<Data>? data;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      appBar: AppBar(
        backgroundColor: Colors.black,
        title: const Text('Trending Movies', style: TextStyle()),
        actions: [
          PopupMenuButton<String>(
            onSelected: (value) => HomeBlocCubit().handleClick(value, context),
            itemBuilder: (BuildContext context) {
              return {'Profil', 'Logout'}.map((String choice) {
                return PopupMenuItem<String>(
                  value: choice,
                  child: Text(choice),
                );
              }).toList();
            },
          ),
        ],
      ),
      body: ScrollConfiguration(
        behavior: const ScrollBehavior().copyWith(overscroll: false),
        child: RefreshIndicator(
          onRefresh: () => context.read<HomeBlocCubit>().fetchingData(context),
          child: Column(
            children: [
              Expanded(
                child: StaggeredGridView.countBuilder(
                  padding: const EdgeInsets.all(10),
                  itemCount: data!.length,
                  scrollDirection: Axis.vertical,
                  physics: const ClampingScrollPhysics(),
                  crossAxisCount: 2,
                  crossAxisSpacing: 5,
                  shrinkWrap: true,
                  staggeredTileBuilder: (int index) =>
                      const StaggeredTile.fit(1),
                  itemBuilder: (context, index) {
                    final dt = data![index];
                    return GestureDetector(
                      onTap: () => Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (_) => MovieDetail(data: dt)),
                      ),
                      behavior: HitTestBehavior.opaque,
                      child: Card(
                        elevation: 5,
                        color: Colors.white10,
                        shape: const RoundedRectangleBorder(
                          borderRadius: BorderRadius.all(Radius.circular(15)),
                        ),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            ClipRRect(
                              borderRadius: const BorderRadius.only(
                                topLeft: Radius.circular(10),
                                topRight: Radius.circular(10),
                              ),
                              child: CachedNetworkImage(
                                imageUrl:
                                    'https://image.tmdb.org/t/p/w500/${dt.posterPath}',
                                placeholder: (context, url) => const Center(
                                    child: CircularProgressIndicator(
                                        color: Colors.red)),
                                errorWidget: (context, url, error) =>
                                    const Icon(Icons.error),
                                fit: BoxFit.cover,
                                width: double.infinity,
                                height: 175,
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.all(10),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    '${dt.title} ${dt.releaseDate == '-' ? '' : '(${dt.releaseDate!.split('-')[0]})'}',
                                    style: const TextStyle(
                                      fontSize: 16,
                                      fontWeight: FontWeight.bold,
                                      color: Colors.white,
                                    ),
                                  ),
                                  const SizedBox(height: 5),
                                  Text(
                                    dt.overview ?? '-',
                                    maxLines: 5,
                                    overflow: TextOverflow.ellipsis,
                                    style: const TextStyle(
                                      fontSize: 12,
                                      color: Colors.white70,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    );
                  },
                ),
              ),
              Container(
                margin: const EdgeInsets.fromLTRB(16, 10, 16, 10),
                child: Row(
                  children: [
                    GestureDetector(
                      onTap: context.read<HomeBlocCubit>().page == 1
                          ? null
                          : () {
                              context.read<HomeBlocCubit>().page--;
                              context.read<HomeBlocCubit>().loadMore(context);
                            },
                      child: Row(
                        children: [
                          Icon(
                            Icons.arrow_back,
                            color: context.read<HomeBlocCubit>().page == 1
                                ? Colors.grey
                                : Colors.red,
                            size: 15,
                          ),
                          const SizedBox(width: 2.5),
                          Text(
                            'Prev',
                            style: TextStyle(
                              color: context.read<HomeBlocCubit>().page == 1
                                  ? Colors.grey
                                  : Colors.red,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ],
                      ),
                    ),
                    const Spacer(),
                    Text(
                      'Page ${context.read<HomeBlocCubit>().page} of 1000',
                      style: const TextStyle(color: Colors.white),
                    ),
                    const Spacer(),
                    GestureDetector(
                      onTap: context.read<HomeBlocCubit>().page == 1000
                          ? null
                          : () {
                              context.read<HomeBlocCubit>().page++;
                              context.read<HomeBlocCubit>().loadMore(context);
                            },
                      child: Row(
                        children: [
                          Text(
                            'Next',
                            style: TextStyle(
                              color: context.read<HomeBlocCubit>().page == 1000
                                  ? Colors.grey
                                  : Colors.blue,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          const SizedBox(width: 2.5),
                          Icon(
                            Icons.arrow_forward,
                            color: context.read<HomeBlocCubit>().page == 1000
                                ? Colors.grey
                                : Colors.blue,
                            size: 15,
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
