import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

import '../models/user.dart';

class UsersDatabase {
  static final UsersDatabase instance = UsersDatabase._init();
  static Database? _database;
  UsersDatabase._init();

  Future<Database?> get database async {
    if (_database != null) return _database;
    _database = await _initDatabase('users.db');
    return _database;
  }

  Future<Database?> _initDatabase(String dbName) async {
    final databasePath = await getDatabasesPath();
    final path = join(databasePath, dbName);
    return await openDatabase(path, version: 1, onCreate: _createDatabase);
  }

  Future<Database?> _createDatabase(Database db, int version) async {
    await db.execute('''
        CREATE TABLE $tableUser (
          id INTEGER PRIMARY KEY AUTOINCREMENT,
          fullname TEXT NOT NULL,
          email TEXT NOT NULL,
          password TEXT NOT NULL
        )
      ''');
    return null;
  }

  Future<User?> register(User user) async {
    final db = await database;
    final maps = await db!.query(
      tableUser,
      columns: ['email'],
      where: 'email = ?',
      whereArgs: [user.email],
    );

    int id;
    if (maps.isNotEmpty) {
      return null;
    } else {
      id = await db.insert(tableUser, user.toJson());
    }
    return user.copy(id: id);
  }

  Future login(String email, String password) async {
    final db = await database;
    final checkEmail = await db!.query(
      tableUser,
      columns: ['email'],
      where: 'email = ?',
      whereArgs: [email],
    );

    final maps = await db.query(
      tableUser,
      columns: ['id', 'fullname', 'email', 'password'],
      where: 'email = ? AND password = ?',
      whereArgs: [email, password],
    );

    if (checkEmail.isEmpty) {
      // email not found
      return 0;
    } else {
      if (maps.isNotEmpty) {
        // login success
        return User.fromJson(maps.first);
      } else {
        // password not match
        return 1;
      }
    }
  }

  Future close() async {
    final db = await database;
    db?.close();
  }
}
