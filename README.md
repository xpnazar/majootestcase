# Majoo Test Case

Project ini merupakan test case dari [Majoo Indonesia](https://majoo.id/) untuk posisi **Mobile Flutter Engineer** dan juga telah dimigrasi ke `null safety`.  
Terdapat `1000 halaman` dengan total `20000 data` yang dapat ditampilkan dari kategori trending dengan jumlah data perhalaman sebanyak `20 data`.  

* Sumber data : [The Movie Database API - Trending](https://developers.themoviedb.org/3/trending/get-trending)  
* Flutter : versi `2.10.3` • `channel stable` • <https://github.com/flutter/flutter.git>  
* Dart : versi `2.16.1`

## Screenshot

Fitur (dari kiri ke kanan) : `login` - `register` - `movie list` - `movie detail` - `modal profile` - `modal logout` - `proses login` - `loading` - `no signal`
![majootestcase](https://user-images.githubusercontent.com/56527536/156933621-a05cfddf-dcf2-4939-86e8-f813012f0821.png)

## Dependencies tambahan

* [cached_network_image](https://pub.dev/packages/cached_network_image) - Simpan cache gambar dari internet
* [intl](https://pub.dev/packages/intl) - Data formatter
* [staggered_grid_view_flutter](https://pub.dev/packages/staggered_grid_view_flutter) - Flexible gridview
* [flutter_lints](https://pub.dev/packages/flutter_lints) - Untuk mengecek coding, tipe data, dan lain-lain

## Ada yang ingin di diskusikan? Hubungi saya melalui

* [LinkedIn](https://www.linkedin.com/in/nazarudin/)
* [WhatsApp](https://wa.me/6281365041803)
